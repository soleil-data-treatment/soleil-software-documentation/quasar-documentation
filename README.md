# Aide et ressources de Quasar pour Synchrotron SOLEIL

[<img src="https://quasar.codes/logo/quasar.png" width="250"/>](https://quasar.codes)

## Résumé

- Acquisition des données, visualisation, prétraîtement
- Open source

## Sources

- Code source: https://github.com/Quasars/orange-spectroscopy
- Documentation officielle: https://quasar.codes/docs/

## Navigation rapide

| Tutoriaux |
| ------ |
| [Tutoriaux officiels Orange](https://github.com/borondics/Orange-Case-Study-Book) |
| [Chaine Youtube officielle Orange](https://www.youtube.com/channel/UClKKWBe2SCAEyv7ZNGhIe4g) |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien), généralement facile mais possibilité conflit avec autres instal Python

## Format de données

- en entrée: csv, spa, map, HDF5 ... gère la majorité des formats commerciaux
- en sortie: ascii ....
- sur un disque dur
